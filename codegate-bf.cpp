#include "widget.h"

#include <GApp>
#include <GJson>

int main(int argc, char *argv[]) {
	GApp a(argc, argv);
	Widget w;

	QJsonObject& jo = GJson::instance();
	jo["widget"] >> w;

	w.show();

	int res = a.exec();

	jo["widget"] << w;

	return res;
}
