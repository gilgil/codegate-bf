QT += core gui widgets

CONFIG += c++17

include(../g/g.pri)

SOURCES += \
	codegate-bf.cpp \
	widget.cpp

HEADERS += \
	widget.h

FORMS += \
	widget.ui

RESOURCES += \
	codegate-bf.qrc
