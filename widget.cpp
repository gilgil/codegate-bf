#include "widget.h"
#include "ui_widget.h"

#include <QMessageBox>

#include <GJson>

Widget::Widget(QWidget *parent)
	: QWidget(parent)
	  , ui(new Ui::Widget)
{
	ui->setupUi(this);
	setWindowTitle("CODEGATE Beacon Flooding");
	ui->plainTextEdit->setReadOnly(true);
	ui->leMessage->setFocus();
	QObject::connect(ui->leMessage, &QLineEdit::returnPressed, this, &Widget::processReturnPressed);
	//ui->lineEdit->setStyleSheet("color: white;  background-color: black");
	//ui->plainTextEdit->setStyleSheet("color: white;  background-color: black");

	updateState();
}

Widget::~Widget()
{
	delete ui;
}

void Widget::updateState() {
	bool active = bf_.active();
	ui->pbStart->setEnabled(!active);
	ui->pbStop->setEnabled(active);
	ui->pbOption->setEnabled(!active);
}

void Widget::propLoad(QJsonObject jo) {
	jo["rect"] >> GJson::rect(this);
	jo["bf"] >> bf_;
}

void Widget::propSave(QJsonObject& jo) {
	jo["rect"] << GJson::rect(this);
	jo["bf"] << bf_;
}

void Widget::processReturnPressed() {
	ui->pbMessage->click();
}

void Widget::on_pbMessage_clicked() {
	QString s = ui->leMessage->text();
	if (s == "") return;
	s = QString::number(index_) + "." + s;
	ui->plainTextEdit->appendPlainText(s);
	bf_.addMessage(s);
	QStringList sl = ui->plainTextEdit->toPlainText().split("\n");
	if (sl.count() > 10) {
		sl.removeAt(0);
		ui->plainTextEdit->setPlainText(sl.join("\n"));
		bf_.removeMessage(0);
	}
	if (++index_ >= 10) {
		index_ = 0;
		bf_.nextMac_ = bf_.startMac_;
	}

	ui->leMessage->setText("");
	ui->leMessage->setFocus();
}

void Widget::on_pbStart_clicked()
{
	GThreadMgr::suspendStart();

	bool isOk = false;
	while (true) {
		bf_.messages_ = ui->plainTextEdit->toPlainText().split("\n");
		if (!bf_.open()) {
			QMessageBox mb;
			mb.warning(this, "Error", bf_.err->msg());
			break;
		}

		ch_.intfName_ = bf_.intfName_;
		ch_.channelList_ = QStringList(QString::number(bf_.channel_));
		if (!ch_.open()) {
			QMessageBox mb;
			mb.warning(this, "Error", ch_.err->msg());
			break;
		}
		isOk = true;
		break;
	}
	if (!isOk) {
		bf_.close();
		ch_.close();
	}
	GThreadMgr::resumeStart();

	updateState();
}

void Widget::on_pbStop_clicked()
{
	bf_.close();
	ch_.close();
	updateState();
}

#include <GPropDialog>
void Widget::on_pbOption_clicked() {
	GPropDialog propDialog;
	propDialog.widget_.setObject(&bf_);

	QJsonObject& jo = GJson::instance();
	bool isFirst = jo.find("propDialog") == jo.end();
	jo["propDialog"] >> propDialog;

	propDialog.show();

	if (isFirst) {
		int width = propDialog.width();
		propDialog.widget_.treeWidget_->setColumnWidth(0, width / 2);
	}

	propDialog.exec();

	jo["propDialog"] << propDialog;
}

