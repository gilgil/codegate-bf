#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <GProp>
#include <GBeaconFlood>
#include <GChannelHop>

QT_BEGIN_NAMESPACE
	namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget, public GProp
{
	Q_OBJECT

public:
	Widget(QWidget *parent = nullptr);
	~Widget() override;

	int index_{0};
	GBeaconFlood bf_;
	GChannelHop ch_;

	void updateState();

public:
	void propLoad(QJsonObject jo) override;
	void propSave(QJsonObject& jo) override;

public slots:
	void processReturnPressed();

private slots:

	void on_pbMessage_clicked();

	void on_pbStart_clicked();

	void on_pbStop_clicked();

	void on_pbOption_clicked();

private:
	Ui::Widget *ui;
};
#endif // WIDGET_H
